package com.aderea.aderea;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.aderea.aderea.helpers.Networking;
import com.aderea.aderea.interfaces.AfterRequest;
import com.aderea.aderea.interfaces.ViewAdOnLayout;
import com.aderea.aderea.models.Adv;
import com.aderea.aderea.models.Device;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class Aderea implements LocationListener , AfterRequest {
    protected LocationManager locationManager;
    private PermissionsHelper permissionsHelper;
    private Location mLocation = null;
    public static String appId,adId ;
    public  String uuid ;
    public Device d;
    public Map<String, Object> bodyInfo;
   public ViewAdOnLayout viewAdOnLayout ;
    SharedPreferences preferences;

    public void init(Context c , ViewAdOnLayout viewAdOnLayout) {
        preferences
                = c.getSharedPreferences("aderea",
                MODE_PRIVATE);

        this.viewAdOnLayout = viewAdOnLayout ;
        Networking networking = new Networking() ;
        try {
            if (ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            ////////////
        } catch (Exception e) {
            e.printStackTrace();
        }
        ///// get device info
        d = new Device();
        d.setDeviceInfo(c);
        bodyInfo = d.getDeviceInfo();
        bodyInfo.put("user_meta", "user_meta") ;
        bodyInfo.put("page_title", "test") ;
        bodyInfo.put("page_url", "test") ;
        bodyInfo.put("page_meta_description", "test") ;
        bodyInfo.put("page_meta_keywords", "test") ;
        bodyInfo.put("page_content_keywords", "test") ;

        /// get location

        locationManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        this.mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (this.mLocation != null) {
            // Do something with the recent location fix
            //  otherwise wait for the update below
            this.setLocationToBody();
             // send ad request
             networking.requestAd(this, this);
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        }

    }

   public  void  setLocationToBody(){
       bodyInfo.put("location_longitude", Double.toString(this.mLocation.getLongitude()));
       bodyInfo.put("location_latitude", Double.toString(this.mLocation.getLatitude()));
     }

    public boolean checkAndRequestPermissions(Context c) {
        permissionsHelper = new PermissionsHelper(viewAdOnLayout);
        return permissionsHelper.checkAndRequestPermissions(c);
    }

    public void onRequestPermissionsResult(Context c, int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsHelper.onRequestPermissionsResult(c, this, requestCode, permissions, grantResults);
    }

    @Override
    public void onLocationChanged(final Location location) {
        if (location != null) {
            this.mLocation = location;
             this.setLocationToBody();
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i("provider", " is : " + provider.toString());
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestAdSuccess(JSONObject response, String type) {
        switch (type){
            case "requestAd":
                try {
                    viewAd(response);
                } catch (JSONException e) {
                    Log.d("JSONException", e.getMessage());
                }

                break;
        }
    }

    public void viewAd(JSONObject response) throws JSONException {
         Adv adv = new Adv() ;
        JSONObject data = response.getJSONObject("data");
        adv.setImg(data.getString("image"));
        adv.setLink(data.getString("link"));
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        int s = Integer.parseInt(sdf.format(new Date()));
        adv.setShow_time(s);
        adv.setShowen(true);

         this.viewAdOnLayout.viewAd(adv);
    }
}
