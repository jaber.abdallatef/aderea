package com.aderea.aderea.interfaces;

import com.aderea.aderea.models.Adv;

public interface ViewAdOnLayout {
     public  void  viewAd(Adv adv) ;
}
