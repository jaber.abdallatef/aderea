package com.aderea.aderea.interfaces;

import com.aderea.aderea.models.Adv;

import org.json.JSONObject;

public interface AfterRequest {
    public  void onRequestAdSuccess(JSONObject response  , String type) ;
}
