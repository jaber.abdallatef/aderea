package com.aderea.aderea.helpers;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JsonHandler {

      public  static    JSONObject   handleJsonResponse(InputStream inputStream) throws IOException, JSONException {
          BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
          StringBuilder sb = new StringBuilder();
          String line;
          while ((line = br.readLine()) != null) {
              sb.append(line+"\n");
          }
          br.close();
          return  new JSONObject(sb.toString());
      }
}
