package com.aderea.aderea.helpers;

import android.os.AsyncTask;
import android.util.Log;

import com.aderea.aderea.interfaces.AfterRequest;
import com.aderea.aderea.models.Adv;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class PostTask extends AsyncTask<String, Void, JSONObject> {
    // This is the JSON body of the post
    JSONObject postData, responseObj;
    AfterRequest afterRequest;
    String requestType;

    // This is a constructor that allows you to pass in the JSON body
    public PostTask(Map<String, Object> postData, String requestType, AfterRequest afterRequest) {
        this.requestType = requestType;
        this.afterRequest = afterRequest;
        if (postData != null) {
            this.postData = new JSONObject(postData);
        }
    }

    // This is a function that we are overriding from AsyncTask.
    // It takes Strings as parameters because that is what we defined for the parameters of our async task
    @Override
    protected JSONObject doInBackground(String... params) {
        responseObj = null;
        try {

            // This is getting the url from the string we passed in
            URL url = new URL(params[0]);
            // Create the urlConnection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");

            // OPTIONAL - Sets an authorization header
            //   urlConnection.setRequestProperty("Authorization", "someAuthString");
            OutputStream out = null;
            // Send the post body

            out = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.write(postData.toString());
            writer.flush();
            writer.close();
            out.close();
            urlConnection.connect();


            int statusCode = urlConnection.getResponseCode();
            if (statusCode == 200) {
                // success
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                responseObj = JsonHandler.handleJsonResponse(inputStream);
                Log.i("jaber", "getString" + responseObj.getString("success"));
            } else {

                // Status code is not 200
                // Do something to handle the error
            }

        } catch (Exception e) {
            Log.i("jaber", e.getMessage());
        }
        return responseObj;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        this.afterRequest.onRequestAdSuccess(jsonObject, requestType);
    }
}