package com.aderea.aderea.helpers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.aderea.aderea.Aderea;
import com.aderea.aderea.interfaces.AfterRequest;
import com.aderea.aderea.models.Device;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class Networking {
    String apiUrl = "http://aderea.com/api/";
    String result;
    public  void requestAd(Aderea aderea , AfterRequest afterRequest) {
        Map<String, Object> postData = new HashMap<>();
         postData  = aderea.bodyInfo  ;
         postData.put("app_id" , aderea.appId);
         postData.put("ad_id" , aderea.adId);
        Log.i("jaber" , postData.toString()) ;
        PostTask task = new PostTask(postData, "requestAd", afterRequest);
        task.execute(apiUrl + "request-ad");
    }


    public static void openWebURL(String inURL, Context c) {
        Intent browse = new Intent(Intent.ACTION_VIEW, Uri.parse(inURL));

        c.startActivity(browse);
    }
}
