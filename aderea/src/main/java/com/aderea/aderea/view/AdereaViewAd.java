package com.aderea.aderea.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;

import androidx.annotation.StyleableRes;

import com.aderea.aderea.R;
import com.aderea.aderea.helpers.DownloadImageTask;
import com.aderea.aderea.helpers.Networking;
import com.aderea.aderea.models.Adv;

public class AdereaViewAd extends LinearLayout {

    @StyleableRes
    int index0 = 0;
    @StyleableRes
    int index1 = 1;
    @StyleableRes
    int index2 = 2;

 private ImageView ad_image ;
    private Adv adv;
    public AdereaViewAd(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.adera_view_ad, this);

        int[] sets = {R.attr.ad_img_src};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, sets);
        typedArray.recycle();

        initComponents();

    }

    private void initComponents() {
       ad_image = (ImageView) findViewById(R.id.ad_img);
    }

    public void setViewData(final Adv adv, final Context c) {
        this.adv = adv;
        new DownloadImageTask((ImageView) ad_image)
                .execute(adv.getImg());
        this.ad_image.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Networking.openWebURL(adv.getLink(), c);
            }
        });
        this.invalidate();
    }


}