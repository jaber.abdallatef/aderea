package com.aderea.aderea.models;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

public class Adv {
    int height, width, time_spent, show_time;
    String type, img, link;
    boolean click = false;
    boolean showen = false;
    public void setImg(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTime_spent() {
        return time_spent;
    }

    public void setTime_spent(int time_spent) {
        this.time_spent = time_spent;
    }

    public int getShow_time() {
        return show_time;
    }

    public void setShow_time(int show_time) {
        this.show_time = show_time;
    }

    public boolean isClick() {
        return click;
    }

    public void setClick(boolean click) {
        this.click = click;
    }

    public boolean isShowen() {
        return showen;
    }

    public void setShowen(boolean showen) {
        this.showen = showen;
    }
}
