package com.aderea.aderea.models;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

public class Device extends AppCompatActivity   {
    private static final int PERMISSION_READ_STATE = 1;
    Platform platform;
    String model;
    String manufacturer;
    String uuid;
    String serial;
    String lang;
    String user;
    int sdk ;
    TelephonyManager tManager ;
    public void setDeviceInfo(Context c) {
         tManager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
        this.platform = new Platform() ;
        this.platform.setNum(Build.VERSION.RELEASE);
        this.platform.setName(this.findAndroidVersionName());
        this.manufacturer = Build.MANUFACTURER;
        this.model = Build.MODEL;
        this.serial = Build.SERIAL;
        this.user = Build.USER;
        this.lang =Locale.getDefault().getDisplayLanguage();
        try{
            this.uuid = tManager.getDeviceId();
        }catch (SecurityException e){
            Log.i("Error" , "Error : " + e.getMessage()) ;
        }
    }

     public Map<String, Object> getDeviceInfo(){
         Map<String, Object> deviceData = new HashMap<>();
         deviceData.put("device_platform", this.platform.name) ;
         deviceData.put("device_model", this.model) ;
         deviceData.put("device_manufacturer", this.manufacturer) ;
         deviceData.put("device_uuid", this.uuid) ;
         deviceData.put("device_version", this.findAndroidVersionName()) ;
         deviceData.put("device_serial", this.serial) ;
         deviceData.put("device_language", this.lang) ;
         deviceData.put("device_isvirtual", false) ;
         return  deviceData ;
     }

     String  findAndroidVersionName(){
         Field[] fields = Build.VERSION_CODES.class.getFields();
         String androidVname = "" ;
         for (Field field : fields) {
             String fieldName = field.getName();
             int fieldValue = -1;
             try {
                 fieldValue = field.getInt(new Object());
             } catch (IllegalArgumentException e) {
                 e.printStackTrace();
             } catch (IllegalAccessException e) {
                 e.printStackTrace();
             } catch (NullPointerException e) {
                 e.printStackTrace();
             }
             if (fieldValue == Build.VERSION.SDK_INT) {
                 androidVname =   fieldName ;
             }
         }
         return  androidVname ;
     }

}
