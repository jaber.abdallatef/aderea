package com.aderea.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.aderea.aderea.Aderea;
import com.aderea.aderea.interfaces.ViewAdOnLayout;
import com.aderea.aderea.models.Adv;
import com.aderea.aderea.view.AdereaViewAd;

public class MainActivity extends AppCompatActivity  implements ViewAdOnLayout {
     public Aderea aderea;
    AdereaViewAd adereaViewAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ///// aderea work
        aderea = new Aderea();
        adereaViewAd = (AdereaViewAd) findViewById(R.id.aderea_view_ad);
        aderea.appId = "d49937fc-ae81-48e3-b5b0-eac25aa2dea2" ;
        aderea.adId = "ea43b70b-9eb8-4b3c-84dd-d2eaceb91330" ;
        /// aderea init
       if(aderea.checkAndRequestPermissions(this))
           aderea.init(this  , this);
        ///=====================
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       aderea.onRequestPermissionsResult(this ,requestCode, permissions, grantResults);
    }

    /////////////// make ad visible and set its data
    @Override
    public void viewAd(Adv adv) {
     this.adereaViewAd.setVisibility(View.VISIBLE);
        this.adereaViewAd.setViewData(adv, this);
    }
}
